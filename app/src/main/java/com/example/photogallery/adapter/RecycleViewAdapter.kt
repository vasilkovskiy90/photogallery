package com.example.photogallery.adapter


import android.content.Context
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.photogallery.R
import com.example.photogallery.common.Images
import com.example.photogallery.utils.CreateBitmap
import kotlinx.android.synthetic.main.recycler_view_item.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class RecycleViewAdapter(
    private val imagesUri: ArrayList<Images>, private val context: Context
) :
    Adapter<RecycleViewAdapter.ViewHolder>() {

    private val tempArray:ArrayList<Bitmap> = ArrayList(1000)

    fun deleteData() {
        imagesUri.removeAll(imagesUri)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val thumbnail: ImageView? = itemView.thumbnail

    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = imagesUri.size

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        GlobalScope.launch {
            val bitmap = CreateBitmap().getBitmap(position, imagesUri, context)
            //.scale(Constants.THUMBNAIL_SIZE, Constants.THUMBNAIL_SIZE)
            val thumbImage = ThumbnailUtils.extractThumbnail(bitmap, 250, 250)
            tempArray.add(thumbImage)
        }

            holder.thumbnail?.scaleType=ImageView.ScaleType.CENTER_CROP
            holder.thumbnail?.setImageBitmap(someInvert(position))

    }
    private fun someInvert(position: Int): Bitmap? {

        if(tempArray.size<=position) {
            return null
        }
        return tempArray[position]
    }
}


