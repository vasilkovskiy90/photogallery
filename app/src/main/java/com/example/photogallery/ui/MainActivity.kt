package com.example.photogallery.ui

import android.app.AlertDialog
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GestureDetectorCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.photogallery.R
import com.example.photogallery.adapter.RecycleViewAdapter
import com.example.photogallery.common.Images
import com.example.photogallery.utils.Constants
import com.example.photogallery.utils.CreateBitmap
import com.example.photogallery.utils.listeners.GestureListener
import com.example.photogallery.utils.listeners.RecyclerItemClickListener
import com.example.photogallery.utils.permissions.CheckPermissions
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {
    var imagesUri: ArrayList<Images> = ArrayList()
    var alert: AlertDialog? = null
    var itemPosition: Int = Constants.FIRST_POSITION
    private lateinit var gestureDetector: GestureDetectorCompat

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initGestureDetector()
        alertDialog()
    }

    private fun initGestureDetector() {
        gestureDetector = GestureDetectorCompat(this,
            GestureListener(main_layout)
        )
        main_layout.setOnTouchListener {v, event ->
            !gestureDetector.onTouchEvent(event)
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onResume() {
        super.onResume()
        CheckPermissions().onCheckPermissions(this)
        imagesUri.clear()
        fetchImagesFromGallery()
        setFirstImage()
    }


    @RequiresApi(Build.VERSION_CODES.P)
    private fun setFirstImage() {
        if (imagesUri.isNotEmpty()) {
            setImageView(Constants.FIRST_POSITION)
        }
    }



    @RequiresApi(Build.VERSION_CODES.Q)
    private fun fetchImagesFromGallery() {
        getAllImagesUri()
        addRecyclerView()
        addRecyclerViewItemListener()
    }

    private fun getAllImagesUri() {
        val uriExternal: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val cursor: Cursor?
        val columnIndexID: Int
        val projection = arrayOf(MediaStore.Images.Media._ID)
        var imageId: Long
        cursor = applicationContext.contentResolver.query(uriExternal, projection, null, null, null)
        if (cursor != null) {
            columnIndexID = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)

            while (cursor.moveToNext()) {
                imageId = cursor.getLong(columnIndexID)
                val imageUri = Uri.withAppendedPath(uriExternal, "" + imageId)
                val images = Images()
                images.imageUri = imageUri
                imagesUri.add(Constants.FIRST_POSITION ,images)
            }
            cursor.close()

        }
    }

    private fun addRecyclerViewItemListener() {
        recyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                this,
                recyclerView,
                object :
                    RecyclerItemClickListener.OnItemClickListener {
                    @RequiresApi(Build.VERSION_CODES.P)
                    override fun onItemClick(view: View, position: Int) {
                        setImageView(position)
                    }

                    override fun onItemLongClick(view: View?, position: Int) {
                        itemPosition = position
                        alert?.show()
                    }
                })
        )
    }

    private fun addRecyclerView() {

        recyclerView.apply {
            val horizontalLayout =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
            layoutManager = horizontalLayout
            adapter = RecycleViewAdapter(
                imagesUri,
                applicationContext
            )

        }
        recyclerView.setHasFixedSize(true)
        recyclerView.setItemViewCacheSize(100)

    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun setImageView(position: Int) {
        val onClickBitmap = CreateBitmap().getBitmap(position, imagesUri, applicationContext)


        bigImage.setImageBitmap(onClickBitmap)
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun alertDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle(applicationContext.getText(R.string.alert_dialog_tittle))
        builder.setMessage(applicationContext.getText(R.string.alert_dialog_text))
        builder.setPositiveButton(
            applicationContext.getText(R.string.alert_dialog_positive)
        ) { dialog, which ->
            imageDelete()
            dialog.dismiss()
        }
        builder.setNegativeButton(
            applicationContext.getText(R.string.alert_dialog_negative)
        ) { dialog, which ->
            dialog.dismiss()
        }
        alert = builder.create()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun imageDelete() {
        applicationContext.grantUriPermission(applicationContext.packageName, imagesUri[itemPosition].imageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        applicationContext.contentResolver.delete(imagesUri[itemPosition].imageUri, null, null)
        RecycleViewAdapter(imagesUri, applicationContext).deleteData()
        fetchImagesFromGallery()
    }

    fun onClickButton(view: View) {
        val cameraIntent = Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA)
        startActivity(cameraIntent)
    }
}

