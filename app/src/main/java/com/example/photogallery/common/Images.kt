package com.example.photogallery.common

import android.net.Uri

data class Images (var imageUri:Uri = Uri.EMPTY)