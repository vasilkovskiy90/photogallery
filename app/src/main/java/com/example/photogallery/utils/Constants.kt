package com.example.photogallery.utils

object Constants {
    const val STORAGE_PERMISSION = 1
    const val THUMBNAIL_SIZE = 250
    const val FIRST_POSITION = 0

}