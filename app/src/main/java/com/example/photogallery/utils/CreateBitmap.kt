package com.example.photogallery.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import androidx.annotation.RequiresApi

import com.example.photogallery.common.Images

class CreateBitmap(){

    @RequiresApi(Build.VERSION_CODES.P)
    fun getBitmap(position: Int,
                  imagePathData: ArrayList<Images>,
                  context: Context):Bitmap{

        val source = ImageDecoder.createSource(context.contentResolver, imagePathData[position].imageUri)
        return ImageDecoder.decodeBitmap(source)

    }


}