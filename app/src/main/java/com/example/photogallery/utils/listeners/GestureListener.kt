package com.example.photogallery.utils.listeners

import android.view.GestureDetector

import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.activity_main.view.*


class GestureListener(private val main_layout: FrameLayout) : GestureDetector.SimpleOnGestureListener() {
var switcher:Boolean = true
    override fun onFling(
        event1: MotionEvent,
        event2: MotionEvent,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        if (switcher){
            main_layout.recyclerView.animate().translationY(main_layout.recyclerView.height.toFloat());
            main_layout.recyclerView.animate().alpha(0.0f);
            main_layout.recyclerView.visibility = View.INVISIBLE

            main_layout.cameraButton.animate().translationY(main_layout.cameraButton.height.toFloat())
            main_layout.cameraButton.animate().alpha(0.0f);
            main_layout.cameraButton.visibility = View.INVISIBLE
            switcher = !switcher
        } else {
            main_layout.recyclerView.animate().alpha(1.0f);
            main_layout.recyclerView.animate().translationY(0F);
            main_layout.recyclerView.visibility = View.VISIBLE

            main_layout.cameraButton.animate().alpha(1.0f);
            main_layout.cameraButton.animate().translationY(0F);
            main_layout.cameraButton.visibility = View.VISIBLE
            switcher = !switcher
        }



        return true
    }
}
